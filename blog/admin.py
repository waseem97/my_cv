from django.contrib import admin
from .models import BlogPost, Images, Category


class BlogPostAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',), }


admin.site.register(BlogPost, BlogPostAdmin)
admin.site.register(Images)
admin.site.register(Category)
from django.shortcuts import render, HttpResponse
from django.views.generic import ListView,  TemplateView
from .models import BlogPost


class BlogListView(ListView):

    template_name = "blog/blogs_list.html"
    model = BlogPost
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context


class BlogDetailsView(TemplateView):

    template_name = "blog/blog_details.html"
    obj = None

    def get(self, request, *args, **kwargs):
        slug = kwargs.get("slug")
        self.obj = BlogPost.objects.filter(slug=slug)
        if not self.obj:
            return HttpResponse("404 Not found")
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["obj"] = self.obj.first()
        return context

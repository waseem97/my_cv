from django.urls import path
from .views import BlogListView, BlogDetailsView

app_name = "blog"

urlpatterns = (
    path("", BlogListView.as_view(), name="blogs_list"),
    path("<str:slug>", BlogDetailsView.as_view(), name="blog_details"),
)
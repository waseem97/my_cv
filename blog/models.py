from django.db import models
from ckeditor.fields import RichTextField


class Images(models.Model):
    ''' 
    Images Album
    each blog post will have a set of images
    '''

    image = models.ImageField("Image", upload_to="album/images/")
    title = models.CharField("Image title", max_length=50, unique=True)
    date = models.DateField(auto_now_add=True)

    class Meta:
        verbose_name = "Image"
        verbose_name_plural = "Images"

    def __str__(self):
        return f"{self.title}"


class Category(models.Model):
    ''' Categories 1:M with the BlogPost '''

    name = models.CharField("Category", max_length=50)
    date = models.DateField(auto_now_add=True)

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

    def __str__(self):
        return f"{self.name}"


class BlogPost(models.Model):
    ''' BLog post model '''

    VISIBILITY_CHOICES = (
        ("VISIBLE", "A published article"),
        ("UNDER_PUBLISHING", "A progresses article"),
        ("HIDDEN", "A hidden article"),
    )

    title = models.CharField("Title", max_length=50)
    content = RichTextField()  # ckeditor
    category = models.ForeignKey(
        verbose_name="Category", to=Category, on_delete=models.CASCADE, related_name="blog_category", null=True)
    date = models.DateField("Creation date", auto_now_add=True)
    cover = models.ImageField("Cover", upload_to="cover/images/", null=True)
    images = models.ManyToManyField(
        verbose_name="Images", to=Images, related_name="blog_images", null=True)
    references = RichTextField(null=True)
    credit = models.CharField("Credit", max_length=255, null=True)
    status = models.CharField("Status", max_length=50,
                              choices=VISIBILITY_CHOICES)

    slug = models.SlugField(max_length=50)
    popular = models.BooleanField("Post on wall", default=False)

    class Meta:
        verbose_name = "Blog post"
        verbose_name_plural = "Blog posts"

    def __str__(self):
        return f"{self.title} at {str(self.date)}"

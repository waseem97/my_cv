from django.contrib import admin
from django.db import models

from .models import MyInfo, SocialAccount, Services, Skills, Portfolio, Testimonial, Experiance, Contact


class ContactAdmin(admin.ModelAdmin):
    list_display = ("sender", "date")


admin.site.register(Contact, ContactAdmin)
admin.site.register(MyInfo)
admin.site.register(SocialAccount)
admin.site.register(Experiance)
admin.site.register(Portfolio)
admin.site.register(Services)
admin.site.register(Skills)
admin.site.register(Testimonial)

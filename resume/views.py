from django.core.checks import messages
from django.db import models
from django.db.models import fields
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.views import generic
from datetime import date
from django.urls import reverse

from .models import (
    MyInfo,
    Skills,
    Portfolio,
    Experiance,
    Services,
    SocialAccount,
    Testimonial,
    Contact
)
# Create your views here.


class ResumeView(generic.TemplateView):
    """ main resuem template """
    template_name = "resume/home.html"

    def get(self, request, *args, **kwargs):

        self.result = kwargs.get("contact_result", None)

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["info"] = MyInfo.objects.first()
        context["skills"] = Skills.objects.all()
        context["portfolio"] = Portfolio.objects.all()
        context["experiances"] = Experiance.objects.all()
        context["services"] = Services.objects.all()
        context["social"] = SocialAccount.objects.all()
        context["testimonilas"] = Testimonial.objects.all()
        context["age"] = date.today().year - context["info"].dob
        context["result"] = self.result

        return context


class ContactView(generic.View):

    def post(self, request, *args, **kwargs):
        sender = request.POST.get("sender")
        email = request.POST.get("email")
        messages = request.POST.get("message")
        new_message = Contact.objects.create(
            sender=sender, email=email, message=messages)
        if new_message:
            return redirect('resume:home')
        else:
            return redirect('resume:home')

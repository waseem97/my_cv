from django.db import models
from django.db.models.fields import CharField, EmailField
from django.db.models.fields.files import ImageField
from utils.model_helpers import SingletonModel


class MyInfo(SingletonModel):

    AVAILABLE = "available"
    NOT_AVAILABLE = "not available"
    HIRED = "hired"

    STATUSES = [
        (AVAILABLE, "available"),
        (NOT_AVAILABLE, "not available"),
        (HIRED, "hired"),

    ]

    name = models.CharField("Your name", max_length=30,
                            null=False, blank=False)
    profile_photo = models.ImageField(
        "Profile Image", upload_to="media", null=True)
    spacialization = models.CharField(
        "Your specialization?", max_length=50, null=False, blank=False)
    brief = models.TextField("Brief about me", null=True, blank=True)
    dob = models.IntegerField("your Date of Birth")
    email = models.EmailField("Your Email")
    phone = models.CharField("Your phone number", max_length=15)
    status = models.CharField(
        "Your current status", max_length=22, choices=STATUSES, default=AVAILABLE)
    resume = models.FileField("Your Reseme File", upload_to="media", null=True)

    class Meta:
        verbose_name = "My Info"

    def __str__(self):
        return f"{self.name} main info"


class SocialAccount(models.Model):

    FACEBOOK = "fb"
    INSTAGRAM = "ig"
    LINKEDIN = "in"
    TWITTER = "tw"

    SOCIALACCOUNTS = [
        (FACEBOOK, "facebook"),
        (INSTAGRAM, "instagram"),
        (LINKEDIN, "linked in"),
        (TWITTER, "twitter"),
    ]

    account = models.CharField(
        "Social platform", max_length=2, choices=SOCIALACCOUNTS)
    profile = models.CharField("Profile name", max_length=22, null=False)
    profile_link = models.URLField("Profile link", null=False)

    class Meta:

        verbose_name = "social account"
        verbose_name_plural = "socail accounts"

    def __str__(self):
        return self.account


class Services(models.Model):

    logo = models.ImageField("logo", upload_to="media", null=True)
    name = models.CharField("service name", max_length=50)

    class Meta:
        verbose_name = "Service"
        verbose_name_plural = "Services"

    def __str__(self):
        return self.name


class Skills(models.Model):

    name = models.CharField("Skill", max_length=22)
    progress = models.IntegerField("Progress", default=0)

    class Meta:

        verbose_name = "Skill"
        verbose_name_plural = "Skills"

    def __str__(self):
        return self.name


class Portfolio(models.Model):

    desc = models.CharField("Portfolio", max_length=30)
    image = models.ImageField("Image", upload_to="media", null=True)
    link = models.CharField("link to the site", max_length=55)

    class Meta:
        verbose_name = "Portfolio"
        verbose_name_plural = "Portfolio"

    def __str__(self):
        return self.desc


class Experiance(models.Model):

    title = models.CharField("Title", max_length=33)
    location = models.CharField("Location", max_length=100)
    brief = models.TextField("Brief",  null=True)
    image = models.ImageField("Image", upload_to="media", null=True)

    class Meta:
        verbose_name = "Expericance"
        verbose_name_plural = "Experiances"

    def __str__(self):
        return self.title


class Testimonial(models.Model):

    name = models.CharField("Name", max_length=50)
    text = models.TextField("Text", null=True)

    class Meta:
        verbose_name = "Testimonial"
        verbose_name_plural = "Testimonials"

    def __str__(self):
        return self.name


class Contact(models.Model):
    '''
    contact me model
    '''
    sender = models.CharField("Sender", max_length=255)
    email = models.EmailField("Email")
    message = models.TextField("Message")
    date = models.DateField(auto_now=True)

    def __str__(self):
        return f"Message from {self.sender} in {str(self.date)}"

    class Meta:
        verbose_name = "Contact"
        verbose_name_plural = "Contacts"

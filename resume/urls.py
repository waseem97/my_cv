from django.urls import path
from .views import ResumeView, ContactView
from django.conf import settings
from django.conf.urls.static import static


app_name = "resume"

urlpatterns = [
    path("", ResumeView.as_view(), name="home"),
    path("contact", ContactView.as_view(), name="contact")
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

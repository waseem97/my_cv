# Generated by Django 3.2.8 on 2021-10-09 09:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('resume', '0002_auto_20211009_0952'),
    ]

    operations = [
        migrations.AlterField(
            model_name='myinfo',
            name='brief',
            field=models.TextField(blank=True, null=True, verbose_name='Brief about me'),
        ),
        migrations.AlterField(
            model_name='myinfo',
            name='dob',
            field=models.IntegerField(verbose_name='your Date of Birth'),
        ),
        migrations.AlterField(
            model_name='myinfo',
            name='email',
            field=models.EmailField(max_length=254, verbose_name='Your Email'),
        ),
        migrations.AlterField(
            model_name='myinfo',
            name='name',
            field=models.CharField(max_length=30, verbose_name='Your name'),
        ),
        migrations.AlterField(
            model_name='myinfo',
            name='phone',
            field=models.CharField(max_length=15, verbose_name='Your phone number'),
        ),
        migrations.AlterField(
            model_name='myinfo',
            name='spacialization',
            field=models.CharField(max_length=50, verbose_name='Your specialization?'),
        ),
        migrations.AlterField(
            model_name='myinfo',
            name='status',
            field=models.CharField(choices=[('available', 'available'), ('not available', 'not available'), ('hired', 'hired')], default='available', max_length=22, verbose_name='Your current status'),
        ),
    ]
